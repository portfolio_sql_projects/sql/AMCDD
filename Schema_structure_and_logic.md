# Database Structure and Logic for MMG Dojo Data

## 1. Overview
The MMG Dojo Data schema is meticulously designed to manage the complex operations 
of the MultiMartial Gang (MMG) martial arts school. This schema facilitates the 
tracking of students, instructors, classes, and martial arts styles, along with the
attendance and progression of students in various disciplines.

## 2. Logic

### 2.1 Tables Description

1. **Students**: Holds details like student number, name, date of birth, and joining date.
2. **Instructors**: Instructors are also listed as students, with additional information such as the start date of their
   instructing role and their status (compensated or volunteer).
3. **Classes**: Details about each class, including level, time, day, and location.
4. **Attendance**: Tracks student attendance for each class session.

### 2.2 Relationships

- Instructors can teach multiple classes and martial arts.
- Students can attend any class of their level and are tracked for their attendance.
- Classes are mapped to specific instructors and times.
- The database also tracks the progression of students through different martial arts ranks.

### 2.3 Functionality

This database is designed to:

- Efficiently manage class schedules and instructor assignments.
- Monitor student attendance and progress in martial arts disciplines.
- Maintain a comprehensive record of all ranks and their requirements.


## 3. Design Considerations

- **Normalization**: The schema is normalized to reduce redundancy and improve data integrity, evident in the separation of student, instructor, class, and rank details into distinct tables.


- **Foreign Key Relationships**: These are used extensively to maintain referential integrity between tables like `classes`, `students`, `instructors`, and `martial_art_styles`.


- **Indexing**: Indices are created for foreign keys to enhance query performance.


- **Auto-Increment IDs**: Used for primary keys to ensure unique identification for records in tables.


- **Character Sets**: UTF-8 character sets support a wide range of characters, accommodating diverse data inputs.


- **CASCADE on Update**: Ensures data consistency across related tables when updates occur.

This schema effectively captures the multifaceted aspects of the MMG dojo, from student enrollment to instructor assignments and class management, ensuring a comprehensive and efficient database system.
