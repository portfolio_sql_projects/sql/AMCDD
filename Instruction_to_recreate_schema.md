# Instruction

### Running the SQL Script

1. **Download the SQL Script**: Ensure that you have downloaded the `MMG_dojo_data_schema.sql` file.

2. **Database Connection**: Open your SQL database management tool (e.g., MySQL Workbench, pgAdmin) and connect to your database server.

3. **Executing the Script**:
    - Locate and open the `MMG_dojo_data_schema.sql` file in your SQL tool.
    - Execute the entire script to create the schema in your database.

### Final Product

After running the script, the following will be set up in your database:

- **Schema Creation**: A new schema named `mmg_dojo_data` is created.
- **Tables**: Various tables are set up to store information about students, instructors, classes, attendance, and more. These tables are designed to manage data regarding:
  - Student details (names, dates of birth, join dates, etc.)
  - Instructor assignments and information.
  - Class schedules, locations, and levels.
  - Student attendance records for each class.
  - Progress tracking of students in different martial arts disciplines.
- **Relationships**: The script establishes relationships between tables, ensuring data integrity and efficient data retrieval.
- **Constraints**: Appropriate constraints are set for primary keys, foreign keys, and unique fields to maintain data accuracy and consistency.


#### Triggers

### `new_student_added`
- **Type**: After Insert Trigger on `students` table.
- **Function**: Assigns the white belt rank to new students upon their addition to the `students` table. It records the student's join date as the date when the rank is awarded. If the join date is unavailable, only the student ID and rank ID are inserted.

#### Views

### `student_info`
- **Purpose**: Provides comprehensive information about students.
- **Details**: Includes student number, first name, last name, date of birth, date joined, martial art style, current belt, and the date the belt was awarded.

### `instructor_info`
- **Purpose**: Offers detailed information about instructors.
- **Details**: Displays instructor's student number, first name, last name, martial art they teach, date of birth, date they joined the school, date assigned as an instructor, and their status (compensated or volunteer).

### `student_class_enrollment_info`
- **Purpose**: Shows classes in which students are enrolled, along with their instructors.
- **Details**: Contains student number, martial arts style, class level (beginner, intermediate, advanced), class timing, day of the week, location, and instructor ID.

### `class_attendance`
- **Purpose**: Tracks student attendance in class meetings.
- **Details**: Includes meeting ID, meeting date, class ID, student number, class time, day, location, attendance status, and instructor ID. Information is primarily ordered by meeting ID and attendance status.


# Note
- Ensure that your database server is running and accessible before executing the script.
- You may need to adjust database connection settings or permissions according to your setup.

# Testing with Sample Data

### Note on Sample Data
- **Randomly Generated Data**: The data inserted by the script is randomly generated and may not follow a logical or chronological order.
- **Data Inconsistencies**: Some data, like student dates or class times, may not be logically consistent or may contradict information in other tables.
- **Purpose of Sample Data**: This randomly generated data is intended solely for testing the logical flow and reactions of the schema. It is not representative of real or accurate information.

### Testing Guidelines
- Run various queries and operations to ensure the schema functions as expected.
- Test the triggers and views to verify they respond correctly to the sample data.
- Observe the system's behavior and note any discrepancies or issues for further refinement.

# Conclusion
Remember, the primary goal of this testing phase is to assess the schema's functionality and not the accuracy of the sample data. Focus on the database's responses and logic under various data conditions.