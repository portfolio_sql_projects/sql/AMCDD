# Introduction to the steps and procedures to create the MMG_dojo_data_schema

## Step 1: Conceptual Model (draw.io)

![MMG Dojo Data Conceptual Model](./models/MMG_dojo_data_conceptual_model.png "MMG_dojo_data_Conceptual Model")

The MMG Dojo Conceptual Model serves as the foundational blueprint for the database system 
of the MultiMartial Gang (MMG) martial arts school. 

This model outlines the critical 
entities that encapsulate the core data points of the school's operations, 
such as students, instructors, classes, and martial art styles. It also illustrates the intricate relationships between these entities,
highlighting the multifaceted interactions within the school's ecosystem. 

Through careful abstraction, the model captures the essence of the school's data 
requirements - from student enrollment and instructor assignments to class 
scheduling and rank progression. Each entity is meticulously defined with relevant 
attributes to ensure a comprehensive representation of the school's data. 

The relationships, denoted by clearly defined associations, ensure a robust 
structure that supports complex queries and operations, pivotal for the efficient
management of the MMG martial arts school database.

### 1.1 Relationships Between Tables

The database for the MMG Dojo captures the complex interrelations of a martial arts school's ecosystem. Below are the key relationships between tables outlined in point form:

- **Students to Classes**: 
  - Students can enroll in multiple classes.
  - A class can have multiple students enrolled.

- **Students to Instructors**:
  - Some students become instructors, establishing a special hierarchical relationship.
  - An instructor is always a student, but not all students are instructors.

- **Instructors to Classes**:
  - Instructors are assigned to teach specific classes.
  - A class is taught by only one instructor, but an instructor can teach multiple classes.

- **Students/Instructors to Class Meetings**:
  - Students attend class meetings, which are specific instances of classes.
  - Instructors teach or assist in class meetings, which may include the head instructor and possibly several assistant instructors.

- **Students to Rank**:
  - Students acquire ranks as they progress in their training.
  - Each rank can be acquired by many students, but a student can hold multiple ranks over time, necessitating the tracking of the rank history per student.

- **Classes to Martial Art Styles**:
  - Each class is associated with a specific martial art style.
  - Martial art styles can be taught in multiple classes, reflecting the variety of classes offered within that style.

- **Class Meetings to Classes**:
  - Class meetings are linked to the class they are a part of.
  - A class will have multiple meetings over time, but each meeting is for a single class.

This network of relationships ensures the database can handle the dynamic and versatile operations of a martial arts school, from scheduling to tracking student progress.


## Step 2: Logical Model (draw.io)
![MMG Dojo Data Logical Model](./models/MMG_dojo_data_logical_model.png "MMG_dojo_data_Logical_Model")

In the progression from a conceptual to a logical model for the MMG dojo database, we refine the structures to be compatible with the MySQL relational database management system. This step is crucial in transitioning from abstract concepts to a practical and implementable database design.

### 2.1 Handling Many-to-Many Relationships in MySQL Workbench

MySQL Workbench requires a more granular approach to relationships.
Connection tables are the standard methodology for representing many-to-many 
relationships within SQL:

- These tables split many-to-many relationships into two one-to-many relationships.
- They simplify the enforcement of referential integrity.
- They make query operations more straightforward and efficient.

By incorporating these logical refinements, it creates a robust blueprint for the 
physical database construction, ensuring that the MMG dojo's data management needs are 
met with a scalable and reliable system.

### 2.2 Key Changes and Concepts

- **Connection Tables**: Introduction of `student_rank`, `student_classes`, and `instructor_classes` to handle many-to-many relationships, a necessity since MySQL does not inherently support these relationships directly.


- **Foreign Keys Specification**: The logical model now explicitly defines foreign keys, such as `StudentID` and `RankID` in `student_rank`, to establish clear relationships between tables.


- **Attribute Detailing**: Data types for each attribute are specified, including `SMALLINT` for integer values, `STRING` for text, and `DATE` for dates, adding precision to the model.


- **Normalization Efforts**: Further normalization of the database structure to ensure data integrity and reduce redundancy, allowing for efficient data management and retrieval.


- **Relationship Clarity**: Enhanced clarity in how entities are related, such as the link between martial art styles and the classes offered, as well as the instructors and their assigned classes.


## Step 3: Creating the Physical Model in MySQL Workbench

![MMG Dojo Data Physical Model](./models/MMG_dojo_data_physical_model.png "MMG_dojo_data_Physical_Model")

Transitioning from the logical model to the physical model involves a series of refinements that prepare the database schema for actual implementation and deployment. The physical model is a direct representation of how data will be stored and accessed in MySQL.

### 3.1 Key Changes from Logical to Physical Model

- **Data Types**: In the physical model, each attribute's data type is selected based on the nature of the data and the storage efficiency. For example, `VARCHAR(50)` for names, `DATE` for dates, and `INT` for numeric identifiers.


- **Primary and Foreign Keys**: The physical model includes explicit primary keys (`PK`) for entity identification and foreign keys (`FK`) for maintaining referential integrity between tables.


- **Indexes**: Indexes are defined on primary keys and other important columns to improve query performance. These might not have been visually represented in the logical model but are crucial for the physical implementation.


- **Table Options**: MySQL-specific options like storage engines (e.g., InnoDB for transaction support), character sets, and collations are applied to optimize the performance and support internationalization.


- **Normalization**: The logical model's normalization is carried out in the physical model to avoid redundancy and ensure data integrity. This may involve creating additional tables or modifying existing ones to achieve normalization forms.


- **Many-to-Many Relationship Resolution**: The physical model uses join tables (such as `student_classes` and `instructor_classes`) to implement many-to-many relationships, as MySQL does not natively support this type of relationship.

### 3.2 Conceptualization to Realization

In MySQL Workbench, the physical model moves beyond conceptual relationships and data structure designs. It involves:

- **Implementing Constraints**: Such as `NOT NULL` and `UNIQUE`, to enforce business rules at the database level.

- **Defining Relationships**: Clearly defining the relationships using foreign keys that were conceptually laid out in the logical model.

- **Setting Auto-Increment Fields**: For tables where a unique identifier is required for each new record, such as `student_id`.

- **Creating Views and Stored Procedures**: If necessary, for common queries or complex operations that are part of the system's functionality.

- **Optimizing Schema**: Ensuring the schema is optimized for the queries that will be run most often, which may involve denormalizing certain parts of the schema for performance reasons.

By completing this step, the database design is ready to be implemented, containing all the necessary SQL code to create tables, relationships, and constraints within the MySQL database.

### 3.3 Forward Engineering
The last step is to forward engineer the physical model in MySQL Workbench to create the schema.
