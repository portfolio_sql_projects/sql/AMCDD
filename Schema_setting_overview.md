# MMG Dojo Data Schema Setting Overview


## 1. Schema Information

- **Schema Name**: `mmg_dojo_data`
- **Character Sets**:
   - `utf8`
   - `utf8mb4`
- **Collations**:
   - `utf8mb4_0900_ai_ci`

### 1.1 Tables and Columns

| Table Name              | Column Name      | Data Type         | Constraints                        | Foreign Key Reference                       |
|-------------------------|------------------|-------------------|------------------------------------|---------------------------------------------|
| martial_art_styles      | style_id         | TINYINT           | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | name             | VARCHAR(50)       | NOT NULL                           |                                             |
| classes                 | class_id         | TINYINT           | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | martial_art_id   | TINYINT           | NOT NULL                           | `martial_art_styles(style_id)`              |
|                         | level            | VARCHAR(25)       | NOT NULL                           |                                             |
|                         | time             | TIME              | NOT NULL                           |                                             |
|                         | day_of_week      | VARCHAR(10)       | NOT NULL                           |                                             |
|                         | location         | VARCHAR(50)       | NOT NULL                           |                                             |
| class_meetings          | meeting_id       | INT               | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | class_id         | TINYINT           | NOT NULL                           | `classes(class_id)`                         |
|                         | date             | DATE              | NOT NULL                           |                                             |
| students                | student_id       | SMALLINT          | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | first_name       | VARCHAR(50)       | NOT NULL                           |                                             |
|                         | last_name        | VARCHAR(50)       | NOT NULL                           |                                             |
|                         | student_number   | VARCHAR(45)       | NOT NULL, UNIQUE                   |                                             |
|                         | date_of_birth    | DATE              | NOT NULL                           |                                             |
|                         | date_joined      | DATE              | NOT NULL                           |                                             |
| instructors             | instructor_id    | TINYINT           | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | student_id       | SMALLINT          | NOT NULL                           | `students(student_id)`                      |
|                         | martial_art_id   | TINYINT           | NOT NULL                           | `martial_art_styles(style_id)`              |
|                         | date_assigned    | DATE              | NOT NULL                           |                                             |
|                         | status           | VARCHAR(10)       | NOT NULL                           |                                             |
| instructor_attendance   | meeting_id       | INT               | PRIMARY KEY                        | `class_meetings(meeting_id)`                |
|                         | instructor_id    | TINYINT           | NOT NULL                           | `instructors(instructor_id)`                |
|                         | role             | VARCHAR(25)       | NOT NULL                           |                                             |
| instructor_classes      | instructor_id    | TINYINT           | NOT NULL                           | `instructors(instructor_id)`                |
|                         | class_id         | TINYINT           | NOT NULL                           | `classes(class_id)`                         |
| ranks                   | rank_id          | TINYINT           | PRIMARY KEY, AUTO_INCREMENT        |                                             |
|                         | belt             | VARCHAR(25)       | NOT NULL                           |                                             |
|                         | description      | VARCHAR(25)       | NOT NULL                           |                                             |
| student_attendance      | meeting_id       | INT               | NOT NULL                           | `class_meetings(meeting_id)`                |
|                         | student_id       | SMALLINT          | NOT NULL                           | `students(student_id)`                      |
|                         | attended         | VARCHAR(10)       | NOT NULL                           |                                             |
| student_classes         | student_id       | SMALLINT          | NOT NULL                           | `students(student_id)`                      |
|                         | class_id         | TINYINT           | NOT NULL                           | `classes(class_id)`                         |
| student_rank            | student_id       | SMALLINT          | NOT NULL                           | `students(student_id)`                      |
|                         | martial_art_id   | TINYINT           | NOT NULL                           | `martial_art_styles(style_id)`              |
|                         | rank_id          | TINYINT           | NOT NULL                           | `ranks(rank_id)`                            |
|                         | date_awarded     | DATE              | NOT NULL                           |                                             |

## 2. Schema creation rundown

### 2.1 Schema Initialization

- **Drop Existing Schema**: The script begins by checking for and dropping the existing `mmg_dojo_data` schema if it exists. This ensures a clean setup.


- **Create New Schema**: It then creates a new schema named `mmg_dojo_data`, setting the character sets to `utf8` and `utf8mb4` respectively, suitable for storing diverse character data.

### 2.2 Table Creation

- **`full_name` Table**: This table is a preliminary setup, likely intended for initial testing or a placeholder.


- **`martial_art_styles` Table**: Stores various martial arts styles offered at MMG, uniquely identified by `style_id`.


- **`classes` Table**: Details each class available, including its martial art style, level, timing, and location. It establishes a relationship with `martial_art_styles`.


- **`class_meetings` Table**: Tracks individual class meetings, linking to the `classes` table.


- **`students` Table**: Records student information, including a unique student number, name, and joining details.


- **`instructors` Table**: Lists instructors who are also students, with additional data like martial art style they teach, and their status. It links to both `students` and `martial_art_styles`.


- **`instructor_attendance` and `instructor_classes` Tables**: Manage instructors' attendance at class meetings and the classes they teach.


- **`ranks` Table**: Defines various martial arts ranks, including their unique identifiers and descriptions.


- **`student_attendance` and `student_classes` Tables**: Keep track of students' attendance in classes and the classes they are enrolled in.


- **`student_rank` Table**: Records the progression of students through different ranks in various martial arts styles.

### 2.3 Final Settings

- Restores the initial SQL mode and key checks to their original state after executing the schema setup.
