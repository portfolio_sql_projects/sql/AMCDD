# MGG Database Design Project

## 1. Introduction

### 1.1 Project Overview

MultiMartial Gang (MMG) needs a database. MMG is a martial arts school with hundreds
of students learning different martial arts including (Taekwondo, Traditional Archery,
Korean sword, Japanese sword, Hung Fist, Wing Chung, Mantis fist… etc. The database 
must keep track of all the classes that are offered, who is assigned to teach each 
class, and which students attend each class. Also, it is important to track the 
progress of each student as they advance.


### 1.2 Technologies Used

This project is designed using SQL Workbench, ensuring a structured and reliable database management system.
Draw.io was also used to designed the Entity Relation Diagram (ERD) of the conceptual and logical models before building
actual physical model in MySQL Workbench.


## 2. Project Requirements

### Student Management
- **Student Information**:
  - Assign a unique student number upon joining.
  - Store name, date of birth, and join date.

### Instructor Management
- **Instructors as Students**:
  - Record all instructors (who are also students).
  - Track the start date of their instructor role and their status (compensated or volunteer).

### Class Management
- **Class Assignments**:
  - Each class has one assigned instructor.
  - Instructors can be assigned multiple classes; volunteer instructors may not have any class assignments.
- **Instructor Expertise**:
  - Instructors can teach multiple martial arts based on their skills and preferences.
- **Class Details**:
  - Define classes by level, time, day, and location (e.g., intermediate-level class in Room 1 on Mondays at 5:00 p.m.).

### Attendance Tracking
- **Student Attendance**:
  - Track attendance for each class meeting.
  - Students can attend any class of their level each week.
- **Class Meeting Dynamics**:
  - Class meetings can vary in student attendance, with some having no attendees.

### Instructor Roles in Classes
- **Instructor Roles in Class Meetings**:
  - Record head and assistant instructors for each class meeting.
  - Note the date and roles of instructors per class meeting.

### Student Rank and Progress
- **Rank Management**:
  - Store rank name, belt color, and requirements.
  - Each rank is associated with specific requirements, except the white belt.
- **Tracking Student Progress Through Ranks**:
  - Record every rank attained by a student.
  - Automatically assign white belt to new students.
  - Maintain the date of awarding each rank to a student.
  - All ranks should have been achieved by at least one student.