USE schema_name;
-- Basic student_infomation --
CREATE OR REPLACE VIEW student_info AS
SELECT 
	student_number,
    first_name,
    last_name,
    date_of_birth,
    date_joined,
	mas.name AS martial_art,
    belt,
    date_awarded
FROM students s
JOIN student_rank sr USING (student_id)
JOIN ranks r USING (rank_id)
JOIN martial_art_styles mas ON mas.style_id = sr.martial_art_id
ORDER BY student_number;



-- -------------------------------------------- --
-- Basic instructor_infomation --
-- -------------------------------------------- --
CREATE OR REPLACE VIEW instructor_info AS
SELECT 
	s.student_number,
    s.first_name,
    s.last_name,
	m.name AS martial_art,
    s.date_of_birth AS DOB,
    s.date_joined AS date_joined_school,
    i.date_assigned AS date_assigned_instructor,
    i.status
FROM students s
JOIN instructors i USING (student_id)
JOIN martial_art_styles m ON m.style_id = i.martial_art_id
ORDER BY i.date_assigned, student_number;



-- -------------------------------------------- --
-- Student_Class info -- # Based on students to present their enrolled classes and corresponding instructors
-- -------------------------------------------- --
CREATE OR REPLACE VIEW student_class_enrollment_info AS
SELECT 
	s.student_number,
    mas.name AS martial_art_styles,
	c.level AS class_level,
    c.time,
    c.day_of_week,
    c.location,
	i.instructor_id
--     st.first_name AS instructor_first_name,
--     st.last_name AS instructor_last_name
FROM students s
JOIN student_classes sc USING (student_id)
JOIN classes c USING (class_id)
JOIN instructor_classes ic USING (class_id)
JOIN instructors i USING (instructor_id)
-- JOIN students st ON st.student_id = i.student_id
JOIN martial_art_styles mas ON mas.style_id = c.martial_art_id
ORDER BY  
    CASE c.level
        WHEN 'beginner' THEN 1
        WHEN 'intermediate' THEN 2
        WHEN 'advance' THEN 3
    END,
	c.time,
    CASE c.day_of_week
		WHEN "Monday" THEN 1
        WHEN "Tuesday" THEN 2
        WHEN "Wednesday" THEN 3
        WHEN "Thursday" THEN 4
        WHEN "Friday" THEN 5
        WHEN "Saturday" THEN 6
        WHEN "Sunday" THEN 7
	END, 
    c.location, 
    s.student_number;



-- -------------------------------------------- --
-- Class_attendance info -- # Based on students to present their enrolled classes and corresponding instructors
-- -------------------------------------------- --
CREATE OR REPLACE VIEW class_attendance AS
SELECT 
	cm.meeting_id,
    cm.date AS meeting_date,
    c.class_id,
	s.student_number,
	c.time,
    c.day_of_week, 
    c.location,
    sa.attended,
    ia.instructor_id	# Get insturctor
--     ia.role AS instructor_role	# Get insturctor
FROM students s
JOIN student_attendance sa USING (student_id)
JOIN class_meetings cm USING (meeting_id)
JOIN classes c USING (class_id)
JOIN instructor_attendance ia USING (meeting_id) 	# Get insturctor
WHERE ia.role = "instructor"
ORDER BY
	cm.meeting_id,
	sa.attended DESC,
	s.student_number 
    

    