-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema schema_name
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema schema_name
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `schema_name` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema schema_name
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema schema_name
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `schema_name` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
-- -----------------------------------------------------
-- Schema schema_name2
-- -----------------------------------------------------
USE `schema_name` ;

-- -----------------------------------------------------
-- Table `schema_name`.`full_name`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`full_name` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

USE `schema_name` ;

-- -----------------------------------------------------
-- Table `schema_name`.`martial_art_styles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`martial_art_styles` (
  `style_id` TINYINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`style_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`classes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`classes` (
  `class_id` TINYINT NOT NULL AUTO_INCREMENT,
  `martial_art_id` TINYINT NOT NULL,
  `level` VARCHAR(25) NOT NULL,
  `time` TIME NOT NULL,
  `day_of_week` VARCHAR(10) NOT NULL,
  `location` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`class_id`, `martial_art_id`),
  INDEX `fk_classes_martial_art_styles1_idx` (`martial_art_id` ASC) VISIBLE,
  CONSTRAINT `fk_classes_martial_art_styles1`
    FOREIGN KEY (`martial_art_id`)
    REFERENCES `schema_name`.`martial_art_styles` (`style_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 43
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`class_meetings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`class_meetings` (
  `meeting_id` INT NOT NULL AUTO_INCREMENT,
  `class_id` TINYINT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`meeting_id`),
  INDEX `fk_class_meetings_classes1_idx` (`class_id` ASC) VISIBLE,
  CONSTRAINT `fk_class_meetings_classes1`
    FOREIGN KEY (`class_id`)
    REFERENCES `schema_name`.`classes` (`class_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 101
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`students` (
  `student_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `student_number` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `date_joined` DATE NOT NULL,
  PRIMARY KEY (`student_id`),
  UNIQUE INDEX `student_number_UNIQUE` (`student_number` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 101
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`instructors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`instructors` (
  `instructor_id` TINYINT NOT NULL AUTO_INCREMENT,
  `student_id` SMALLINT NOT NULL,
  `martial_art_id` TINYINT NOT NULL,
  `date_assigned` DATE NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`instructor_id`, `student_id`, `martial_art_id`),
  INDEX `fk_instructors_students1_idx` (`student_id` ASC) VISIBLE,
  INDEX `fk_instructors_martial_art_styles1_idx` (`martial_art_id` ASC) VISIBLE,
  CONSTRAINT `fk_instructors_martial_art_styles1`
    FOREIGN KEY (`martial_art_id`)
    REFERENCES `schema_name`.`martial_art_styles` (`style_id`),
  CONSTRAINT `fk_instructors_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `schema_name`.`students` (`student_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`instructor_attendance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`instructor_attendance` (
  `meeting_id` INT NOT NULL,
  `instructor_id` TINYINT NOT NULL,
  `role` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`meeting_id`, `instructor_id`),
  INDEX `fk_instructor_attendance_class_meetings1_idx` (`meeting_id` ASC) VISIBLE,
  INDEX `fk_instructor_attendance_instructors1_idx` (`instructor_id` ASC) VISIBLE,
  CONSTRAINT `fk_instructor_attendance_class_meetings1`
    FOREIGN KEY (`meeting_id`)
    REFERENCES `schema_name`.`class_meetings` (`meeting_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_instructor_attendance_instructors1`
    FOREIGN KEY (`instructor_id`)
    REFERENCES `schema_name`.`instructors` (`instructor_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 63
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`instructor_classes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`instructor_classes` (
  `instructor_id` TINYINT NOT NULL,
  `class_id` TINYINT NOT NULL,
  INDEX `fk_instructor_classes_instructors1_idx` (`instructor_id` ASC) VISIBLE,
  INDEX `fk_instructor_classes_classes1_idx` (`class_id` ASC) VISIBLE,
  PRIMARY KEY (`instructor_id`, `class_id`),
  CONSTRAINT `fk_instructor_classes_classes1`
    FOREIGN KEY (`class_id`)
    REFERENCES `schema_name`.`classes` (`class_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_instructor_classes_instructors1`
    FOREIGN KEY (`instructor_id`)
    REFERENCES `schema_name`.`instructors` (`instructor_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`ranks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`ranks` (
  `rank_id` TINYINT NOT NULL AUTO_INCREMENT,
  `belt` VARCHAR(25) NOT NULL,
  `description` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`rank_id`),
  UNIQUE INDEX `rank_id_UNIQUE` (`rank_id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`student_attendance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`student_attendance` (
  `meeting_id` INT NOT NULL,
  `student_id` SMALLINT NOT NULL,
  `attended` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`meeting_id`, `student_id`),
  INDEX `fk_student_attendance_students1_idx` (`student_id` ASC) VISIBLE,
  INDEX `fk_student_attendance_class_meetings1_idx` (`meeting_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_attendance_class_meetings1`
    FOREIGN KEY (`meeting_id`)
    REFERENCES `schema_name`.`class_meetings` (`meeting_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_student_attendance_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `schema_name`.`students` (`student_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 201
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`student_classes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`student_classes` (
  `student_id` SMALLINT NOT NULL,
  `class_id` TINYINT NOT NULL,
  INDEX `fk_student_classes_students1_idx` (`student_id` ASC) VISIBLE,
  INDEX `fk_student_classes_classes1_idx` (`class_id` ASC) VISIBLE,
  PRIMARY KEY (`student_id`, `class_id`),
  CONSTRAINT `fk_student_classes_classes1`
    FOREIGN KEY (`class_id`)
    REFERENCES `schema_name`.`classes` (`class_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_student_classes_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `schema_name`.`students` (`student_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schema_name`.`student_rank`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schema_name`.`student_rank` (
  `student_id` SMALLINT NOT NULL,
  `martial_art_id` TINYINT NOT NULL,
  `rank_id` TINYINT NOT NULL,
  `date_awarded` DATE NOT NULL,
  INDEX `fk_student_rank_ranks_idx` (`rank_id` ASC) VISIBLE,
  INDEX `fk_student_rank_students1_idx` (`student_id` ASC) VISIBLE,
  PRIMARY KEY (`student_id`, `rank_id`, `martial_art_id`),
  INDEX `fk_student_rank_martial_art_styles1_idx` (`martial_art_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_rank_ranks`
    FOREIGN KEY (`rank_id`)
    REFERENCES `schema_name`.`ranks` (`rank_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_student_rank_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `schema_name`.`students` (`student_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_student_rank_martial_art_styles1`
    FOREIGN KEY (`martial_art_id`)
    REFERENCES `schema_name`.`martial_art_styles` (`style_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

USE `schema_name` ;

-- -----------------------------------------------------
-- procedure Generaschema_nameudentRanks
-- -----------------------------------------------------

DELIMITER $$
USE `schema_name`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Generaschema_nameudentRanks`()
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE student_id_val INT;
    DECLARE rank_id_val INT;
    DECLARE date_awarded_val DATE;
    
    -- Cursor to select student_ids and their rank_ids
    DECLARE cur CURSOR FOR
        SELECT student_id, rank_id
        FROM Student_Rank
        ORDER BY student_id, rank_id DESC; -- Order by rank_id in descending order
    
    -- Cursor handler
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    
    read_loop: LOOP
        FETCH cur INTO student_id_val, rank_id_val;
        
        IF done THEN
            LEAVE read_loop;
        END IF;
        
        -- Insert records for each rank in descending order with date_awarded
        SET date_awarded_val = (SELECT date_joined FROM Students WHERE student_id = student_id_val);
        
        WHILE rank_id_val > 0 DO
            INSERT INTO Student_Rank (student_id, rank_id, date_awarded)
            VALUES (student_id_val, rank_id_val, date_awarded_val);
            
            SET rank_id_val = rank_id_val - 1;
            SET date_awarded_val = DATE_SUB(date_awarded_val, INTERVAL FLOOR(RAND() * 90) DAY); -- Adjust the interval as needed
        END WHILE;
    END LOOP;
    
    CLOSE cur;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
