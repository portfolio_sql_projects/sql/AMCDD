USE bolo;
DROP TABLE IF EXISTS bolo.full_name;
CREATE TABLE `bolo`.`full_name` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(50) NOT NULL,
  `lastname` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('1', 'Riley', 'Thompson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('2', 'Lucas', 'Anderson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('3', 'Lucas', 'Jones');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('4', 'Carter', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('5', 'William', 'Davis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('6', 'Elijah', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('7', 'Henry', 'Jackson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('8', 'Avery', 'Hall');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('9', 'Sofia', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('10', 'Amelia', 'Moore');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('11', 'Ava', 'Green');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('12', 'Alexander', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('13', 'Isabella', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('14', 'Penelope', 'Garcia');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('15', 'Ethan', 'Hernandez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('16', 'James', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('17', 'Scarlett', 'Taylor');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('18', 'Camila', 'King');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('19', 'Aiden', 'Davis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('20', 'Madison', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('21', 'Harper', 'Jones');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('22', 'Aiden', 'Baker');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('23', 'Camila', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('24', 'Sofia', 'Thompson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('25', 'Scarlett', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('26', 'Logan', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('27', 'Aiden', 'Moore');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('28', 'James', 'Davis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('29', 'Olivia', 'Rodriguez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('30', 'Mia', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('31', 'Charlotte', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('32', 'Sebastian', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('33', 'James', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('34', 'David', 'Jackson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('35', 'Noah', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('36', 'Alexander', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('37', 'Ava', 'Robinson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('38', 'Scarlett', 'Flores');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('39', 'Michael', 'Perez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('40', 'Mia', 'Harris');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('41', 'Aria', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('42', 'Grace', 'Johnson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('43', 'Benjamin', 'Wright');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('44', 'Madison', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('45', 'Sofia', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('46', 'Henry', 'Torres');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('47', 'James', 'Jackson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('48', 'Noah', 'Miller');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('49', 'Michael', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('50', 'Wyatt', 'Brown');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('51', 'Emma', 'Lee');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('52', 'Oliver', 'Gonzalez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('53', 'Isabella', 'Thompson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('54', 'Olivia', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('55', 'Logan', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('56', 'Grace', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('57', 'Noah', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('58', 'David', 'Clark');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('59', 'Jackson', 'King');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('60', 'Emma', 'Sanchez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('61', 'Grace', 'Baker');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('62', 'Aria', 'Green');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('63', 'Alexander', 'Young');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('64', 'William', 'Taylor');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('65', 'Grace', 'Hernandez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('66', 'Liam', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('67', 'David', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('68', 'Mason', 'Garcia');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('69', 'Ethan', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('70', 'Evelyn', 'Anderson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('71', 'Michael', 'Green');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('72', 'Noah', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('73', 'Avery', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('74', 'Penelope', 'Smith');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('75', 'Ava', 'Smith');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('76', 'Daniel', 'Young');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('77', 'Joseph', 'Davis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('78', 'Wyatt', 'Hall');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('79', 'Ethan', 'Martin');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('80', 'Chloe', 'Flores');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('81', 'Alexander', 'Robinson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('82', 'Sebastian', 'Martinez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('83', 'Samuel', 'Hall');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('84', 'Amelia', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('85', 'Abigail', 'Johnson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('86', 'Henry', 'Garcia');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('87', 'Joseph', 'Garcia');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('88', 'Daniel', 'Harris');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('89', 'Penelope', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('90', 'Scarlett', 'Flores');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('91', 'Sebastian', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('92', 'Sofia', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('93', 'Charlotte', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('94', 'Sofia', 'Gonzalez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('95', 'Aiden', 'Young');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('96', 'Emma', 'Taylor');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('97', 'Abigail', 'Lee');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('98', 'Harper', 'Martin');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('99', 'Camila', 'Jackson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('100', 'Aiden', 'Harris');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('101', 'Grace', 'Smith');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('102', 'Harper', 'Gonzalez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('103', 'Jacob', 'Brown');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('104', 'Alexander', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('105', 'Henry', 'Young');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('106', 'Sebastian', 'Martin');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('107', 'Sofia', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('108', 'Isabella', 'Nelson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('109', 'Isabella', 'Martin');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('110', 'Sofia', 'Allen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('111', 'Sebastian', 'Allen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('112', 'James', 'Flores');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('113', 'Amelia', 'Rodriguez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('114', 'Chloe', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('115', 'Penelope', 'Nelson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('116', 'Emma', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('117', 'Sebastian', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('118', 'Noah', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('119', 'Logan', 'Nguyen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('120', 'Joseph', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('121', 'Alexander', 'Allen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('122', 'Lucas', 'Sanchez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('123', 'Ella', 'Clark');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('124', 'Jacob', 'White');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('125', 'Joseph', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('126', 'Abigail', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('127', 'Riley', 'Thomas');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('128', 'James', 'Clark');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('129', 'Victoria', 'Scott');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('130', 'Evelyn', 'Thompson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('131', 'Emma', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('132', 'Jackson', 'Torres');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('133', 'Mason', 'Nguyen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('134', 'David', 'Miller');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('135', 'Ella', 'Lewis');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('136', 'Logan', 'Hill');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('137', 'Aiden', 'Scott');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('138', 'Avery', 'Campbell');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('139', 'Lucas', 'Nguyen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('140', 'Daniel', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('141', 'Elizabeth', 'Hill');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('142', 'Harper', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('143', 'Matthew', 'Nguyen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('144', 'Jackson', 'Hill');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('145', 'Evelyn', 'Green');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('146', 'Benjamin', 'Wright');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('147', 'Mason', 'Sanchez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('148', 'Liam', 'Walker');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('149', 'Ella', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('150', 'Ethan', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('151', 'Elijah', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('152', 'Michael', 'Hall');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('153', 'Grace', 'Robinson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('154', 'Charlotte', 'Martinez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('155', 'Aiden', 'Lopez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('156', 'Madison', 'Sanchez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('157', 'Ethan', 'Allen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('158', 'Ava', 'Hernandez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('159', 'Abigail', 'Clark');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('160', 'Emma', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('161', 'Matthew', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('162', 'Jacob', 'Gonzalez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('163', 'Michael', 'Perez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('164', 'Matthew', 'Allen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('165', 'Michael', 'Smith');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('166', 'Avery', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('167', 'Madison', 'Jones');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('168', 'Carter', 'Hernandez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('169', 'Michael', 'Moore');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('170', 'Olivia', 'Rodriguez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('171', 'Jackson', 'Nguyen');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('172', 'Jackson', 'Thomas');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('173', 'Benjamin', 'Harris');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('174', 'Michael', 'Ramirez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('175', 'James', 'Garcia');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('176', 'Sebastian', 'Young');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('177', 'Scarlett', 'Baker');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('178', 'William', 'Moore');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('179', 'Victoria', 'King');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('180', 'Emily', 'Martin');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('181', 'Emma', 'Baker');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('182', 'Michael', 'King');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('183', 'Isabella', 'White');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('184', 'Olivia', 'Thompson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('185', 'Alexander', 'Hill');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('186', 'Abigail', 'Wilson');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('187', 'Jackson', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('188', 'Charlotte', 'Harris');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('189', 'Alexander', 'Gonzalez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('190', 'Olivia', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('191', 'Olivia', 'Adams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('192', 'Abigail', 'Green');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('193', 'Abigail', 'Clark');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('194', 'Aria', 'Martinez');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('195', 'Michael', 'Rivera');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('196', 'Carter', 'Jones');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('197', 'Charlotte', 'Williams');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('198', 'Ella', 'Smith');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('199', 'Jacob', 'Jones');
INSERT INTO `bolo`.`full_name` (`id`, `firstname`, `lastname`) VALUES ('200', 'Victoria', 'Young');



-- Generating 100 records with random dates between 2010 and current year
# students table
USE bolo;

DELETE FROM students;

ALTER TABLE students AUTO_INCREMENT = 1;

INSERT INTO students (first_name, last_name, student_number, date_of_birth, date_joined)
SELECT 
	(SELECT firstname FROM full_name  ORDER BY RAND() LIMIT 1),
    (SELECT lastname FROM full_name  ORDER BY RAND() LIMIT 1),
--     (SELECT first_name FROM first_name ORDER BY RAND() LIMIT 1),
--     (SELECT last_name FROM last_name ORDER BY RAND() LIMIT 1),
    LPAD(SEQ.SeqVal, 5, '0'),
    DATE_SUB(CURDATE(), INTERVAL FLOOR(RAND() * 25) + 10 YEAR), -- Date of birth at least 10 years earlier
    DATE(CONCAT(FLOOR(RAND() * 14) + 2010, '-', LPAD(FLOOR(RAND() * 12) + 1, 2, '0'), '-', LPAD(FLOOR(RAND() * 28) + 1, 2, '0'))) -- Date joined between 2010 and 2023
FROM (
    SELECT ones.SeqVal + tens.SeqVal * 10 + hundreds.SeqVal * 100 + thousands.SeqVal * 1000 AS SeqVal
    FROM
        (SELECT 0 AS SeqVal UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) ones
    CROSS JOIN
        (SELECT 0 AS SeqVal UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) tens
    CROSS JOIN
        (SELECT 0 AS SeqVal UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) hundreds
    CROSS JOIN
        (SELECT 1 AS SeqVal UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) thousands
) SEQ
LIMIT 100;



# ranks table
USE bolo;
DELETE FROM ranks;
ALTER TABLE ranks AUTO_INCREMENT = 1;

INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('white', 'beginner');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('orange', 'beginner');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('yellow', 'intermediate');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('green', 'intermediate');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('blue', 'advance');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('brown', 'advance');
INSERT INTO `bolo`.`ranks` (`belt`, `description`) VALUES ('black', 'master');



# martial_art_styles table
USE bolo;
DELETE FROM martial_art_styles;
ALTER TABLE martial_art_styles AUTO_INCREMENT = 1;

INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Hung Fist');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Mantis Fist');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Taekwando');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Karate');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Haidong Gumdo');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Archery');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Wing Chung');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Battojusu');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Iaido');
INSERT INTO `bolo`.`martial_art_styles` (`name`) VALUES ('Shaolin Kung Fu');



# classes table
USE bolo;
DELETE FROM classes;
ALTER TABLE classes AUTO_INCREMENT = 1;

INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'beginner', '17:00', 'Monday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'beginner', '17:00', 'Monday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'beginner', '17:00', 'Monday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'intermediate', '18:30', 'Monday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'intermediate', '18:30', 'Monday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'intermediate', '18:30', 'Monday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'beginner', '17:00', 'Tuesday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'beginner', '17:00', 'Tuesday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'beginner', '17:00', 'Tuesday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'intermediate', '18:30', 'Tuesday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'intermediate', '18:30', 'Tuesday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'intermediate', '18:30', 'Tuesday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'beginner', '17:00', 'Wednesday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'beginner', '17:00', 'Wednesday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'beginner', '17:00', 'Wednesday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'intermediate', '18:30:00', 'Wednesday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'intermediate', '18:30:00', 'Wednesday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'intermediate', '18:30:00', 'Wednesday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'intermediate', '17:00', 'Thursday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'intermediate', '17:00', 'Thursday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'intermediate', '17:00', 'Thursday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'advance', '18:30:00', 'Thursday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'advance', '18:30:00', 'Thursday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'advance', '18:30:00', 'Thursday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'beginner', '17:00', 'Friday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'beginner', '17:00', 'Friday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'beginner', '17:00', 'Friday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'advance', '18:30:00', 'Friday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'advance', '18:30:00', 'Friday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'advance', '18:30:00', 'Friday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'advance', '10:00', 'Saturday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'advance', '10:00', 'Saturday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'advance', '13:00', 'Saturday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'advance', '13:00', 'Saturday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'advance', '15:00', 'Saturday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'advance', '15:00', 'Saturday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('1', 'intermediate', '17:00', 'Saturday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('2', 'intermediate', '17:00', 'Saturday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('3', 'beginner', '17:00', 'Saturday', 'room3');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('4', 'intermediate', '18:30', 'Saturday', 'room1');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('5', 'intermediate', '18:30', 'Saturday', 'room2');
INSERT INTO `bolo`.`classes` (`martial_art_id`, `level`, `time`, `day_of_week`, `location`) VALUES ('6', 'beginner', '18:30', 'Saturday', 'room3');



# student_rank table
USE bolo;
DELETE FROM student_rank;

INSERT INTO student_rank (student_id, martial_art_id, rank_id, date_awarded)
SELECT
    s.student_id,
    FLOOR(1 + RAND() * 6) AS martial_art_id,  -- Randomly assign rank_ids between 1 and 7 for all students
    FLOOR(1 + RAND() * 7) AS rank_id,
    CASE
        WHEN FLOOR(1 + RAND() * 7) = 1 THEN s.date_joined  -- Set date_awarded = date_joined for student_id = 1
        ELSE DATE_ADD(s.date_joined, INTERVAL FLOOR(RAND() * 365) DAY)  -- Random date for other rank_ids
    END AS date_awarded
FROM students s;

-- Create Views -------------------------------------
-- DELIMITER //
-- DROP PROCEDURE IF EXISTS GeneratestudentRanks;
-- CREATE PROCEDURE GeneratestudentRanks()
-- BEGIN
--     DECLARE done INT DEFAULT FALSE;
--     DECLARE student_id_val INT;
--     DECLARE rank_id_val INT;
--     DECLARE date_awarded_val DATE;
--     
--     -- Cursor to select student_ids and their rank_ids
--     DECLARE cur CURSOR FOR
--         SELECT student_id, rank_id
--         FROM student_rank
--         ORDER BY student_id, rank_id DESC; -- Order by rank_id in descending order
--     
--     -- Cursor handler
--     DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
--     
--     OPEN cur;
--     
--     read_loop: LOOP
--         FETCH cur INTO student_id_val, rank_id_val;
--         
--         IF done THEN
--             LEAVE read_loop;
--         END IF;
--         
--         -- Set the date_awarded value
--         SET date_awarded_val = (SELECT date_joined FROM Students WHERE student_id = student_id_val);
--         
--         WHILE rank_id_val > 0 DO
--             -- Check if the record already exists
--             IF NOT EXISTS(SELECT 1 FROM Student_Rank WHERE student_id = student_id_val AND rank_id = rank_id_val) THEN
--                 -- Insert new record
--                 INSERT INTO Student_Rank (student_id, rank_id, date_awarded)
--                 VALUES (student_id_val, rank_id_val, date_awarded_val);
--             END IF;
--             
--             SET rank_id_val = rank_id_val - 1;
--             SET date_awarded_val = DATE_SUB(date_awarded_val, INTERVAL FLOOR(RAND() * 90) DAY); -- Adjust the interval as needed
--         END WHILE;
--     END LOOP;
--     
--     CLOSE cur;
-- END//

-- DELIMITER ;
-- End Views ------------------

-- CALL GeneratestudentRanks(); -- call views




# class_meetings
DELETE FROM class_meetings;
ALTER TABLE class_meetings AUTO_INCREMENT = 1;

INSERT INTO class_meetings (class_id, date)
SELECT
    CEIL(RAND() * 42) AS class_id, -- Random class_id between 1 and 42
    DATE_ADD('2010-01-01', INTERVAL FLOOR(RAND() * 4748) DAY) AS date -- Random date between 2010 and 2023
FROM
    (SELECT 1 AS n UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4
     UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8
     UNION ALL SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11 UNION ALL SELECT 12
     UNION ALL SELECT 13 UNION ALL SELECT 14 UNION ALL SELECT 15 UNION ALL SELECT 16
     UNION ALL SELECT 17 UNION ALL SELECT 18 UNION ALL SELECT 19 UNION ALL SELECT 20
     UNION ALL SELECT 21 UNION ALL SELECT 22 UNION ALL SELECT 23 UNION ALL SELECT 24
     UNION ALL SELECT 25 UNION ALL SELECT 26 UNION ALL SELECT 27 UNION ALL SELECT 28
     UNION ALL SELECT 29 UNION ALL SELECT 30 UNION ALL SELECT 31 UNION ALL SELECT 32
     UNION ALL SELECT 33 UNION ALL SELECT 34 UNION ALL SELECT 35 UNION ALL SELECT 36
     UNION ALL SELECT 37 UNION ALL SELECT 38 UNION ALL SELECT 39 UNION ALL SELECT 40
     UNION ALL SELECT 41 UNION ALL SELECT 42 UNION ALL SELECT 43 UNION ALL SELECT 44 
     UNION ALL SELECT 45 UNION ALL SELECT 46
     UNION ALL SELECT 47 UNION ALL SELECT 48 UNION ALL SELECT 49 UNION ALL SELECT 50
     UNION ALL SELECT 51 UNION ALL SELECT 52 UNION ALL SELECT 53 UNION ALL SELECT 54
     UNION ALL SELECT 55 UNION ALL SELECT 56 UNION ALL SELECT 57 UNION ALL SELECT 58
     UNION ALL SELECT 59 UNION ALL SELECT 60 UNION ALL SELECT 61 UNION ALL SELECT 62
     UNION ALL SELECT 63 UNION ALL SELECT 64 UNION ALL SELECT 65 UNION ALL SELECT 66
     UNION ALL SELECT 67 UNION ALL SELECT 68 UNION ALL SELECT 69 UNION ALL SELECT 70
     UNION ALL SELECT 71 UNION ALL SELECT 72 UNION ALL SELECT 73 UNION ALL SELECT 74
     UNION ALL SELECT 75 UNION ALL SELECT 76 UNION ALL SELECT 77 UNION ALL SELECT 78
     UNION ALL SELECT 79 UNION ALL SELECT 80 UNION ALL SELECT 81 UNION ALL SELECT 82
     UNION ALL SELECT 83 UNION ALL SELECT 84 UNION ALL SELECT 85 UNION ALL SELECT 86
     UNION ALL SELECT 87 UNION ALL SELECT 88 UNION ALL SELECT 89 UNION ALL SELECT 90
     UNION ALL SELECT 91 UNION ALL SELECT 92 UNION ALL SELECT 93 UNION ALL SELECT 94
     UNION ALL SELECT 95 UNION ALL SELECT 96 UNION ALL SELECT 97 UNION ALL SELECT 98
     UNION ALL SELECT 99 UNION ALL SELECT 100
    ) AS numbers
LIMIT 200;



# student_attendance 
DELETE FROM student_attendance;
ALTER TABLE student_attendance AUTO_INCREMENT = 1;

INSERT INTO student_attendance (student_id, meeting_id, attended)
SELECT
    s.student_id, -- Random student_id between 1 and 100
    m.meeting_id, -- Random meeting_id between 1 and 100
    IF(RAND() > 0.5, 'yes', 'no') AS attended -- Random 'yes' or 'no' for attended column
FROM
    (SELECT student_id FROM students ORDER BY RAND() LIMIT 200) s -- Randomly select 200 student_ids
CROSS JOIN
    (SELECT meeting_id FROM class_meetings ORDER BY RAND() LIMIT 200) m -- Randomly select 200 meeting_ids
LIMIT 200; 


# student_classes
DELETE FROM student_classes;
ALTER TABLE student_classes AUTO_INCREMENT = 1;

INSERT INTO student_classes (student_id, class_id)
SELECT
    s.student_id, -- Random student_id between 1 and 100
    c.class_id -- Each class_id repeated 20 times
FROM (
    SELECT DISTINCT student_id FROM students -- Get distinct student_id values
) s
CROSS JOIN (
    SELECT class_id FROM classes ORDER BY RAND() LIMIT 200 -- Randomly select 200 class_ids
) c
LIMIT 200; 

# instructors table
DELETE FROM instructors;
ALTER TABLE instructors AUTO_INCREMENT = 1;

INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('2', '1', '2018-02-03', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('6', '3', '2022-03-04', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('16', '5', '2023-05-06', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('27', '6', '2023-09-12', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('29', '4', '2023-07-18', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('35', '2', '2022-09-18', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('44', '1', '2023-11-26', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('46', '3', '2021-09-22', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('50', '3', '2022-08-11', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('53', '2', '2023-10-09', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('57', '5', '2019-03-22', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('61', '5', '2017-01-18', 'assistance');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('66', '6', '2015-11-27', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('68', '6', '2023-12-11', 'instructor');
INSERT INTO `bolo`.`instructors` (`student_id`, `martial_art_id`, `date_assigned`, `status`) VALUES ('72', '1', '2016-04-12', 'assistance');



# instructor_classes
DELETE FROM instructor_classes;

INSERT INTO instructor_classes (instructor_id, class_id)
SELECT
    i.instructor_id,
    c.class_id
FROM
    (SELECT instructor_id, martial_art_id FROM instructors WHERE status = 'instructor') i
JOIN
    (SELECT class_id, martial_art_id FROM classes ORDER BY RAND() LIMIT 42) c
ON
    i.martial_art_id = c.martial_art_id;
    
    

# instructor_attendance
DELETE FROM instructor_attendance;
ALTER TABLE instructor_attendance AUTO_INCREMENT = 1;

INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '5', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('3', '50', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('4', '32', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '90', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '7', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '8', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '9', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('9', '2', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('10', '1', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('2', '3', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '6', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '9', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '17', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('8', '54', 'colunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '53', 'colunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '52', 'colunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '51', 'colunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('2', '50', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('3', '49', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('4', '48', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '57', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '47', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '46', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('8', '45', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '44', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('10', '43', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('13', '42', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('14', '41', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('14', '42', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('12', '41', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('12', '47', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '50', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '60', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('2', '70', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('3', '70', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('4', '8', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '6', 'volunteer');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '3', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '77', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('8', '78', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('9', '79', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('10', '80', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('12', '80', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('14', '82', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('13', '84', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('15', '86', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('15', '88', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '90', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '91', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('4', '33', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('8', '32', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('9', '34', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('2', '25', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('3', '27', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '29', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '30', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('1', '99', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '4', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('5', '2', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('6', '88', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('7', '69', 'instructor');
INSERT INTO `bolo`.`instructor_attendance` (`instructor_id`, `meeting_id`, `role`) VALUES ('8', '96', 'instructor');
