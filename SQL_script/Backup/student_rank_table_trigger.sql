USE mmg_student_instructor_rank_class_info;
DROP TRIGGER IF EXISTS set_date_awarded;
DROP TRIGGER IF EXISTS new_student_added;
DELIMITER //

CREATE TRIGGER new_student_added
AFTER INSERT ON students
FOR EACH ROW
BEGIN
    DECLARE joined_date DATE;

    SELECT s.date_joined INTO joined_date
    FROM students s
    JOIN ranks r ON r.rank_id = 1
    WHERE s.student_id = NEW.student_id LIMIT 1;

    IF joined_date IS NOT NULL THEN
        INSERT INTO student_rank (student_id, rank_id, date_awarded)
        VALUES (NEW.student_id, 1, joined_date);
    ELSE
        INSERT INTO student_rank (student_id, rank_id)
        VALUES (NEW.student_id, 1);
    END IF;
END;
//

DELIMITER ;
SHOW TRIGGERS;